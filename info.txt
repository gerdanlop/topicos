Generador de Datos de Prueba

Construya un programa que genere Ordenes de Compra de prueba en UBL a partir de un listado de Empresas (Party) e Items,
El programa recibe por entrada el numero de Orders que debe generar y al final mostrar el tiempo total que le tomo generar todos los documentos.
Cada documento Order debe generarse siguiendo las siguientes reglas:

    -- LISTO --
    ID: que sea una cadena de 10 caracteres
    UUID: que sea un GUID valido
    IssueDate: que sea una fecha valida
    
    -- LISTO --
    El SellerSupplierParty se reemplazan cada uno  por un Party tomado del archivo parties.xml aleatoriament
    BuyerCustomerParty se reemplazan cada uno  por un Party tomado del archivo parties.xml aleatoriamente
    
    -- LISTO --
    Se debe generar minimo 1 y maximo 10 elementos OrderLine por cada Orden
    En cada uno se debe asignar los siguientes valores tomados aleatoriamente del archivo items.xml:
    Description
    PriceAmount
    Quantity: Poner un valor aleatorio (entre 0 y 100 )
    LineExtensionAmount: la multiplicacion de PriceAmount * Quantity

    -- LISTO --
    Llenar los elemento 
    AnticipatedMonetaryTotal
    LineExtensionAmount
    PayableAmount 
    con la sumatoria de todos los LineExtensionAmount de los OrderLine 

    -- LISTO --
    Cada Order resultante se debe guardar en un archivo cuyo nombre sea el ID o el UUID de la Orden