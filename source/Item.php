<?php


class Item{
        
    public $id = '';
    public $priceAmount = '';
    public $description = '';
    public $quantity = 0;
    
    function __construct($id, $priceAmount, $description) {
        $this->id = $id;
        $this->priceAmount = $priceAmount;
        $this->description = $description;
        $this->quantity = rand(1,100);
    }
    
    private function format($name){
        return "<tr><td>{$name}</td></tr>";
    }
    
    public function __toString() {
        return  "<div class='col-md-4'><table class='table table-striped table-hovered table-bordered'>"
                . "<tr><th>Item</th></tr>"
                . $this->format($this->id)
                . $this->format($this->priceAmount)
                . $this->format($this->description)
                . "</table></div>";       
    }


}

