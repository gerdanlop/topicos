<?php



class XMLManager{
    
    const items = 'xml/items.xml';
    const parties = 'xml/parties.xml';
    const orders = 'xml/Order-template.xml';
    
    public static function listItems(){
        $listItems = [];
        $doc = new DOMDocument();
        $doc->load(self::items);
        $searchNode = $doc->getElementsByTagName( "CatalogueLine" );
        foreach( $searchNode as $node ){ 
            $listNodeId = $node->getElementsByTagName( "ID" ); 
            $listNodePriceAmount = $node->getElementsByTagName("RequiredItemLocationQuantity")->item(0)->getElementsByTagName("Price")->item(0)->getElementsByTagName("PriceAmount");
            $listNodeDescription = $node->getElementsByTagName("Item")->item(0)->getElementsByTagName("Description");
            $id = $listNodeId->item(0)->nodeValue;
            $priceAmount = $listNodePriceAmount->item(0)->nodeValue;
            $description = $listNodeDescription->item(0)->nodeValue;
            $item = new Item($id,$priceAmount,$description);
            array_push($listItems,$item);
        }
        return $listItems;
    }
    
    public static function listParties(){
        $listParties = [];
        $doc = new DOMDocument();
        $doc->load(self::parties);
        $searchNode = $doc->getElementsByTagName( "Party" );
        foreach( $searchNode as $node ){ 
            $listNodepartyName = $node->getElementsByTagName( "PartyName" )->item(0)->getElementsByTagName("Name"); 
            $listNodePostalAddress = $node->getElementsByTagName( "PostalAddress" );  
            $listNodeContact =  $node->getElementsByTagName( "Contact" );
            $countrySubEntity = $listNodePostalAddress->item(0)->getElementsByTagName("CountrySubentity")->item(0)->nodeValue;
            $addressLine =  $listNodePostalAddress->item(0)->getElementsByTagName("AddressLine")->item(0)->getElementsByTagName("Line")->item(0)->nodeValue;
            $countryName =  $listNodePostalAddress->item(0)->getElementsByTagName("Country")->item(0)->getElementsByTagName("Name")->item(0)->nodeValue;
            $name = $listNodepartyName->item(0)->nodeValue;
            $postalZone = $listNodePostalAddress->item(0)->getElementsByTagName("PostalZone")->item(0)->nodeValue;
            $contactName = $listNodeContact->item(0)->getElementsByTagName("Name")->item(0)->nodeValue;
            $contactTel = $listNodeContact->item(0)->getElementsByTagName("Telephone")->item(0)->nodeValue;
            $contactFax = $listNodeContact->item(0)->getElementsByTagName("Telefax")->item(0)->nodeValue;
            $contactNote = $listNodeContact->item(0)->getElementsByTagName("Note")->item(0)->nodeValue;
            $Party = new Party($name, $postalZone, $countrySubEntity, $addressLine, $countryName, $contactName, $contactTel, $contactFax, $contactNote);
            array_push($listParties,$Party);
        }
        return $listParties;
    }
    
    public static function orderTemplate(){
        $DOM = new DOMDocument();
        $DOM->load(self::orders);
        return $DOM;
    }
    
    public static function itemsToSave($quantityItems){ 
        /*DOM variable*/
        $dom = self::orderTemplate();
        
        /*Collections*/
        $XMLItemArray = [];
        $itemArray = self::listItems();
        
        /*Order variable*/
        $totalAmount = 0;

        for ($i = 1; $i <= $quantityItems ; $i++){
            /*Rnd collection position*/
            $pos = rand(0,count($itemArray));
            
            /*Item values*/
            $quantityValue = $itemArray[$pos]->quantity;
            $descriptionValue = $itemArray[$pos]->description;
            $priceAmount = $itemArray[$pos]->priceAmount;
            $baseQuantityValue = 1;

            /*sum value to order*/
            $totalAmount=$totalAmount+$priceAmount;
            
            /*value to DOM value*/
            $quantityDOM = $dom->createTextNode($quantityValue);
            $descriptionDOM = $dom->createTextNode($descriptionValue);
            $priceAmountDOM = $dom->createTextNode($priceAmount);
            $baseQuantityDOM = $dom->createTextNode($baseQuantityValue);

            /*define XML Tags*/
            $lineItemTag = $dom->createElement('cac:LineItem');
            $quantityTag = $dom->createElement('cbc:Quantity');
            $lineExtensionAmountTag = $dom->createElement('cbc:LineExtensionAmount');
            $priceTag = $dom->createElement('cac:Price');
            $priceAmountTag = $dom->createElement('cbc:PriceAmount');
            $baseQuantityTag = $dom->createElement('cbc:BaseQuantity');
            $itemTag = $dom->createElement('cac:Item');
            $descriptionTag = $dom->createElement('cbc:Description');

            /*define XML Attributes*/
            $quantityTag->setAttribute('unitCode','UND');
            $baseQuantityTag->setAttribute('unitCode','UND');
            $lineExtensionAmountTag->setAttribute('currencyID','USD');
            $priceAmountTag->setAttribute('currencyID','USD');

            /*add DOM values to Tags*/
            $quantityTag->appendChild($quantityDOM);
            $descriptionTag->appendChild($descriptionDOM);
            $baseQuantityTag->appendChild($baseQuantityDOM);
            $priceAmountTag->appendChild($priceAmountDOM);

            /*Mix internal hierarchies*/
            $priceTag->appendChild($priceAmountTag);
            $priceTag->appendChild($baseQuantityTag);
            $itemTag->appendChild($descriptionTag);

            /*Mix external hierarchies*/
            $lineItemTag->appendChild($lineExtensionAmountTag);
            $lineItemTag->appendChild($quantityTag);
            $lineItemTag->appendChild($priceTag);
            $lineItemTag->appendChild($itemTag);
            
            /*add XMLitem to XMLitemArray*/
            array_push($XMLItemArray,$lineItemTag);
        }
        
        /*return XMLitemArray with quantity length*/
        return ['XMLItemArray'=>$XMLItemArray,'totalAmount'=>$totalAmount,'XMLDOM'=>$dom];
    }
        

    
}
