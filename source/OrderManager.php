<?php

class OrderManager  extends Thread{
    
    const path = 'orders/';
    const linkPath = '/topicos/orders/';
    
    public function __construct($ordersQuantity = 0){     
      
        $this->createFiles($ordersQuantity);
    }
    
    public function listFiles(){
        $counter = 1;
        $dir = opendir(self::path);
        while ($elemento = readdir($dir)){
            if( $elemento != "." && $elemento != ".."){
                if( !is_dir(self::path.$elemento) ){
                    echo $this->format($counter,self::linkPath.$elemento);
                    $counter++;
                }
            }
        }
        closedir($dir);
    }
        
    public function removeFiles(){
        $dir = opendir(self::path);
        while ($elemento = readdir($dir)){
            if( $elemento != "." && $elemento != ".."){
                if( !is_dir(self::path.$elemento) ){
                    unlink(self::path.$elemento);
                }
            }
        }
        closedir($dir);
    }
    
    public function format($name,$path){
        return "<div class='col-md-4'>
                    <h2>Order {$name}</h2>
                    <p>Generated Order</p>
                    <p>
                        <a class='btn btn-default' href='{$path}' role='button'>View details &raquo;</a>
                    </p>
                </div>\n";                  
    }
    
    public function createFiles($quantity = 0){
        for ($i = 1 ; $i <= $quantity ; $i++) {
            $Order = new Order();
        }
    }
    
}

