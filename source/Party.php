<?php

class Party{
       
    public $name = '';
    public $postalZone = '';
    public $countrySubEntity = '';
    public $addressLine = '';
    public $countryName = '';
    public $contactName = '';
    public $telephone = '';
    public $telefax = '';
    public $note = '';
    
    public function __construct($name, $postalZone, $countrySubEntity , $addressLine, $countryName, $contactName, $telephone, $telefax, $note) {
        $this->name = $name;
        $this->postalZone = $postalZone;
        ($countrySubEntity === '')? $this->countrySubEntity = 'N/A' : $this->countrySubEntity = $countrySubEntity;
        $this->addressLine = $addressLine;
        $this->countryName = $countryName;
        $this->contactName = $contactName;
        $this->telephone = $telephone;
        ($telefax === '')?$this->telefax = 'N/A' : $this->telefax = $telefax;
        $this->note = $note;
    }
    
    private function format($name){
        return "<tr><td>{$name}</td></tr>";
    }
    
    public function __toString() {
        return  "<div class='col-md-4'><table class='table table-striped table-hovered table-bordered'>"
                . "<tr><th>Party</th></tr>"
                . $this->format($this->name)
                . $this->format($this->postalZone)
                . $this->format($this->countrySubEntity)
                . $this->format($this->addressLine)
                . $this->format($this->countryName)
                . $this->format($this->contactName)
                . $this->format($this->telephone)
                . $this->format($this->telefax)
                . $this->format($this->note)
                . "</table></div>";
    }


    
    
}