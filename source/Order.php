<?php

class Order{
    
    public $fileName = 'Order-?.xml';
    /* Order common info */
    public $uuid = '';
    public $id = '';
    public $issueDate = '';
    public $totalAmount = '';
    /* Seller and Buyer Objects */
    public $buyerCustomerParty = '';
    public $sellerCustomerParty = '';
    /* BuyerInfo */
    public $buyerPartyName = '';
    public $buyerPostalName = '';
    public $buyerPostalZone = '';
    public $buyerCountrySubEntity = '';
    public $buyerAddressLine = '';
    public $buyerCountryName = '';
    public $buyerContactName = '';
    public $buyerContactPhon = '';
    public $buyerContactFax = '';
    public $buyerContactNote = '';
    /* Seller info*/
    public $sellerPartyName = '';
    public $sellerPostalName = '';
    public $sellerPostalZone = '';
    public $sellerCountrySubEntity = '';
    public $sellerAddressLine = '';
    public $sellerCountryName = '';
    public $sellerContactName = '';
    public $sellerContactPhon = '';
    public $sellerContactFax = '';
    public $sellerContactNote = ''; 
    /* XML Arrays*/
    public $arrayOfParties;
    public $arrayOfXMLItems;
    /* XML DOM*/
    private $xmlDom = NULL;
    
    public function __construct(){
        /* Construct variables*/
        $vectorXML = XMLManager::itemsToSave(rand(1,10));
        $this->arrayOfXmlItems = $vectorXML['XMLItemArray'];
        $this->arrayOfParties = XMLManager::listParties();
        $this->xmlDom = $vectorXML['XMLDOM'];
        /* simple variables*/
        $rnd = Randomize::rndPos($this->arrayOfParties);
        /* Common order info */
        $this->uuid = Randomize::UUID();
        $this->id = Randomize::ID();
        $this->issueDate = Randomize::ISSUEDATE();
        $this->fileName = preg_replace('/\?/',$this->id,$this->fileName);
        $this->totalAmount = $vectorXML['totalAmount'];
        /*Buyer and Seller info */
        $this->buyerCustomerParty = $this->arrayOfParties[$rnd['Buyer']];
        $this->sellerCustomerParty = $this->arrayOfParties[$rnd['Seller']];
        $this->buyerPartyName = $this->buyerCustomerParty->name;
        $this->buyerPostalName = $this->buyerCustomerParty->postalZone;
        $this->buyerCountrySubEntity = $this->buyerCustomerParty->countrySubEntity;
        $this->buyerAddressLine = $this->buyerCustomerParty->addressLine;
        $this->buyerCountryName = $this->buyerCustomerParty->countryName;
        $this->buyerContactName = $this->buyerCustomerParty->contactName;
        $this->buyerContactPhon = $this->buyerCustomerParty->telephone;
        $this->buyerContactFax = $this->buyerCustomerParty->telefax;
        $this->buyerContactNote = $this->buyerCustomerParty->note;
        $this->sellerPartyName = $this->sellerCustomerParty->name;
        $this->sellerPostalName = $this->sellerCustomerParty->postalZone;
        $this->sellerCountrySubEntity = $this->sellerCustomerParty->countrySubEntity;
        $this->sellerAddressLine = $this->sellerCustomerParty->addressLine;
        $this->sellerCountryName = $this->sellerCustomerParty->countryName;
        $this->sellerContactName = $this->sellerCustomerParty->contactName;
        $this->sellerContactPhon = $this->sellerCustomerParty->telephone;
        $this->sellerContactFax = $this->sellerCustomerParty->telefax;
        $this->sellerContactNote = $this->sellerCustomerParty->note;
        $this->storage($this->xmlDom);
    }
    
    private function storage($dom){    
        /* select dynamic nodes*/
        $idNode = $dom->firstChild->childNodes->item(3);
        $uuidNode = $dom->firstChild->childNodes->item(7);
        $dateNode = $dom->firstChild->childNodes->item(9);
        $buyerPartyNode = $dom->firstChild->childNodes->item(13)->childNodes->item(1);
        $sellerPartyNode = $dom->firstChild->childNodes->item(15)->childNodes->item(1);
        $anticipatedMonetaryNode = $dom->firstChild->childNodes->item(21);
        $itemsNode = $dom->firstChild->childNodes->item(23);
        
        /* set XML order info*/
        $idNode->nodeValue = $this->id;
        $uuidNode->nodeValue = $this->uuid;
        $dateNode->nodeValue = $this->issueDate;
        $anticipatedMonetaryNode->childNodes->item(1)->nodeValue = $this->totalAmount;
        $anticipatedMonetaryNode->childNodes->item(3)->nodeValue = $this->totalAmount;
        
        /*set XML buyer info */
        $buyerPartyNode->childNodes->item(1)->nodeValue = $this->buyerPartyName;
        $buyerPartyNode->childNodes->item(3)->childNodes->item(1)->nodeValue = $this->buyerPostalName;
        $buyerPartyNode->childNodes->item(3)->childNodes->item(3)->nodeValue = $this->buyerPostalZone;
        $buyerPartyNode->childNodes->item(3)->childNodes->item(5)->nodeValue = $this->buyerCountrySubEntity;
        $buyerPartyNode->childNodes->item(3)->childNodes->item(7)->childNodes->item(1)->nodeValue = $this->buyerAddressLine;
        $buyerPartyNode->childNodes->item(3)->childNodes->item(9)->childNodes->item(1)->nodeValue = $this->buyerCountryName;
        $buyerPartyNode->childNodes->item(5)->childNodes->item(1)->nodeValue = $this->buyerContactName;
        $buyerPartyNode->childNodes->item(5)->childNodes->item(3)->nodeValue = $this->buyerContactPhon;
        $buyerPartyNode->childNodes->item(5)->childNodes->item(5)->nodeValue = $this->buyerContactFax;
        $buyerPartyNode->childNodes->item(5)->childNodes->item(7)->nodeValue = $this->buyerContactNote;

        /*set XML seller info */
        $sellerPartyNode->childNodes->item(1)->nodeValue = $this->sellerPartyName;
        $sellerPartyNode->childNodes->item(3)->childNodes->item(1)->nodeValue = $this->sellerPostalName;
        $sellerPartyNode->childNodes->item(3)->childNodes->item(3)->nodeValue = $this->sellerPostalZone;
        $sellerPartyNode->childNodes->item(3)->childNodes->item(5)->nodeValue = $this->sellerCountrySubEntity;
        $sellerPartyNode->childNodes->item(3)->childNodes->item(7)->childNodes->item(1)->nodeValue = $this->sellerAddressLine;
        $sellerPartyNode->childNodes->item(3)->childNodes->item(9)->childNodes->item(1)->nodeValue = $this->sellerCountryName;
        $sellerPartyNode->childNodes->item(5)->childNodes->item(1)->nodeValue = $this->sellerContactName;
        $sellerPartyNode->childNodes->item(5)->childNodes->item(3)->nodeValue = $this->sellerContactPhon;
        $sellerPartyNode->childNodes->item(5)->childNodes->item(5)->nodeValue = $this->sellerContactFax;
        $sellerPartyNode->childNodes->item(5)->childNodes->item(7)->nodeValue = $this->sellerContactNote;
        
        /*Remove template item*/
        $nodeAux = $itemsNode->childNodes->item(1);
        $itemsNode->removeChild($nodeAux);
        
        /*save XML random items*/
        foreach($this->arrayOfXmlItems as $tmpItem){
            $registeredItem = $itemsNode->appendChild($tmpItem);
            $dom->saveXML($registeredItem);
        }
        
        $dom->save(__dir__.'/../orders/'.$this->fileName);
        
    }
    
    
}