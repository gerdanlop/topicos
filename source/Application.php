<?php

/* Util Class*/
require 'Randomize.php';

/* Class Controllers */
require 'OrderManager.php';
require 'XMLManager.php';
require 'TimeManager.php';

/* Domain Class*/
require 'Order.php';
require 'Party.php';
require 'Item.php';

class Application {
     
    const msjQuantityError = "<div class='alert alert-warning'>Quantity can not be ZERO</div>";
 
    public static function Execute(){
	
		   error_reporting(E_ERROR);
	
	
	
        //Time App controller
        $TimeManager = new TimeManager();
        //Trigger GET vars show = [items, parties] quantity = [1-n]
        self::generateItems();
        self::generateOrdersTemplate();
        self::generateParties();
        //Show time
        echo $TimeManager->end();
    }
    
    private static function generateOrdersTemplate(){
        if(isset($_GET['quantity'])){
            if(!is_array($_GET['quantity'])){
                if(is_numeric($_GET['quantity'])){
                    if($_GET['quantity']>=1){
                        // Borrar archivos 
                        $OrderManager = new OrderManager($_GET['quantity']);                        
                        $OrderManager->removeFiles();
                        // Crear hilos 
                 /**       $cant = $_GET['quantity'];                        
                        $OrderManager1 = new OrderManager($cant / 4);
                        $OrderManager1->start();  
                        
                        $OrderManager2 = new OrderManager($cant / 4);
                        $OrderManager2->start();  
                        
                        
                        $OrderManager3 = new OrderManager($cant / 4);
                        $OrderManager3->start();  
                        
                        $OrderManager4 = new OrderManager($cant / 4);
                        $OrderManager4->start();  
                        
                        
                        $OrderManager1->join();
                        $OrderManager2->join();
                        $OrderManager3->join();
                        $OrderManager4->join();
                        **/
                        
                        $OrderManager->listFiles();
                    }else{
                        echo self::msjQuantityError;
                    }
                }else{
                    echo self::msjQuantityError;
                }
            }
        }
    }

    public static function generateItems(){
        if(isset($_GET['show'])){
            if(!is_array($_GET['show'])){
                if($_GET['show']==='items'){
                    $items = XMLManager::listItems();
                    foreach($items as $val){
                        echo $val->__toString();
                    }
                }   
            }
        }
    }
    
    public static function generateParties(){
        if(isset($_GET['show'])){
            if(!is_array($_GET['show'])){
                if($_GET['show']==='parties'){
                    $parties = XMLManager::listParties();
                    foreach($parties as $val){
                        echo $val->__toString();
                    }
                }   
            }
        }
    }
    
    
    
}

