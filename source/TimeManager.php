<?php

class TimeManager {
    
    public $INIT_TIME = NULL;
    public $FINAL_TIME = NULL;
    
    
    public function __construct() {
        date_default_timezone_set ('America/Bogota');
        $this->INIT_TIME = $this->microtime_float();
    }
    
    public function end(){
        $this->FINAL_TIME = $this->microtime_float();
        return $this->toString();
    }
	
	public function microtime_float(){
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
	}
    
    private function interval(){
        $interval = $this->FINAL_TIME - $this->INIT_TIME;
        return $interval;
    }
    
    private function format(){
        $this->INIT_TIME->format($format);
    }
    
    public function toString(){
        return '</div><div class="row"><div class="alert alert-default"> <b>Start time:</b> '.date("l jS F \@ g:i a",$this->INIT_TIME).'<br/>'.
               '<b>End time:</b>  '.date("l jS F \@ g:i a",$this->FINAL_TIME).'<br/>'.
               '<b>Total time:</b> '.$this->interval().' Microsegundos<br/>'.
			   '<b>Total time:</b> '.($this->interval()/1000000). ' Segundos</div>';
    }
    
    
    
}
