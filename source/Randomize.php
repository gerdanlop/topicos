<?php

final class Randomize{
    
    /*
     *  FUNCIONES 
     *  static ID(): retornar una cadena de 10 numeros EX: 32165465464
     *  static UUID(): retornar un GUID valido EX:6E09886B-DC6E-439F-82D1-7CCAC7F4E3B1
     *  static ISSUEDATE(): retornar una fecha valida EX: 2015-06-20
     *  static randomString(@param1): retorna una cadena de caracteres de la longitud asignada
     *  @param1 = longitud de respuesta
     */
    
    public static function ID(){
        return rand(32000000000,62000000000).rand(0,9);
    }
    
    public static function UUID(){
        $p1 = self::randomString(8);
        $p2 = self::randomString(4);
        $p3 = self::randomString(4);
        $p4 = self::randomString(4);
        $p5 = self::randomString(12);
        return $p1.'-'.$p2.'-'.$p3.'-'.$p4.'-'.$p5;
    }
    
    public static function ISSUEDATE(){
        $Date = new DateTime('now');
        $randomDate = mt_rand($Date->getTimestamp(),$Date->getTimestamp()+15000000);   
        $newRanDate = date("Y-m-d",$randomDate);
        return $newRanDate;
    }
    
    private static function randomString($len){
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $len; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
    private static function RandomPosition($collection){
        $x = count($collection);
        return rand(0,$x-1);
    }

    public static function rndPos($collection){
        $x = self::RandomPosition($collection);
        $y = self::RandomPosition($collection);
        if($x == $y){
            while($x == $y){
                $y = Randomize::RandomPosition($collection);
            }
        }
        return ['Buyer'=>$x,'Seller'=>$y];
    }
}

